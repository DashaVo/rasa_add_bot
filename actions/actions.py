from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction,Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet

def clean_name(name):
    return "".join([c for c in name if c.isalpha()])

def clean_num(name):
    return "".join([c for c in name if c.isalnum()])

class ActionSaySum(Action):

    def name(self) -> Text:
        return "action_say_sum"

    def run(self, dispatcher: CollectingDispatcher,
        tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        first_num =int(clean_num(tracker.get_slot("first_num")))
        last_num = int(clean_num(tracker.get_slot("last_num")))
        sum = first_num + last_num
        dispatcher.utter_message(text=f"Result {first_num} + {last_num} = {sum} ")
        return [SlotSet("last_num",None),SlotSet("first_num",None)]

class ValidateNameForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_name_form"

    def validate_user_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `first_num` value."""

        # If the name is super short, it might be wrong.
        name = clean_name(slot_value)
        if len(name) < 3 :
            dispatcher.utter_message(text="That must've been more then 3.")
            return {"user_name": None}
        return {"user_name": name}

class ValidateAddForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_add_form"

    def validate_first_num(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `first_num` value."""

        # If the name is super short, it might be wrong.
        num = clean_name(slot_value)
        if len(num) <= 0 :
            dispatcher.utter_message(text="That must've been more then 3.")
            return {"first_num": None}
        return {"first_num": num}
   
    def validate_last_num(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `last_num` value."""

        # If the name is super short, it might be wrong.
        num = clean_name(slot_value)
        if len(num) <= 0 :
            dispatcher.utter_message(text="That must've been more then 3.")
            return {"last_num": None}
        return {"last_num": num}